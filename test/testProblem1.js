const {
  createDirectory,
  createRandomFiles,
  deleteRandomFiles,
} = require("../problem1");

let directory = "folder";
function testFunction() {
  try {
    //create directory and create random json files and delete random json files
    createDirectory(directory)
      .then(() => {
        let createPromises = [];
        for (let i = 0; i <= 6; i++) {
          let randomFile = `./${directory}/file-${i}.json`;
          createPromises.push(
            createRandomFiles(randomFile, "data").then(() =>
              deleteRandomFiles(randomFile)
            )
          );
        }
        return Promise.all(createPromises);
      })
      .then(() => {
        console.log("All files created and deleted successfully");
      })
      .catch((err) => {
        console.log(err);
      });
  } catch (error) {
    console.log(error);
  }
}

testFunction();
