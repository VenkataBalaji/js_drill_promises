const fs = require("fs").promises;

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

function createDirectory(directoryPath) {

   return fs.mkdir(directoryPath);
 
}
function createRandomFiles(fileNames, data) {
 
   return  fs.writeFile(fileNames, data);
 
}
function deleteRandomFiles(files) {

return  fs.unlink(files);
  
}

module.exports = { createDirectory, createRandomFiles, deleteRandomFiles };
